# coding: latin-1

# # Vers�o 2
# # Redu��o de dados, nesta vers�o permitimos que fosse fornecido como entrada
# # figuras em cada filtro individualmente, deixando a possibilidade de apenas
# # dois filtros serem analisados, ao inv�s de ter apenas a op��o de an�lise
# # de tr�s filtros.

# # Vers�o 3
# # Adicionar a condi��o de �ndices J e K dentro do intervalo de validade da
# # f�rmula de convers�o.

# # Vers�o 4
# # Usada para quando tiver figuras de c�u e de objeto somadas.

# # Vers�o 5
# # Aceita um n�mero indefinido de imagens para an�lise
# # O input.txt dever� seguir com o diret�rio <espa�o> arquivos separados por
# # <espa�o>.
# # Sempre na ordem diret�rio\ arquivo_1 arquivo_2

# # Vers�ao 5.1
# # Alterado o alcance para sintonizar o brilho e o contraste

# # Vers�o 6
# # Aceita um n�mero indefinido de imagens para an�lise
# # O input.txt dever� seguir com o diret�rio <espa�o> arquivos separados por
# # <espa�o>.
# # Sempre na ordem diret�rio\ arquivo_c�u1 arquivo_objeto1 arquivo_c�u2
# # arquivo_objeto2 ...

# # Vers�ao 6.1
# # Alterado o alcance para sintonizar o brilho e o contraste

# # Redu��o de dados manual
import sys
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
from scipy import ndimage
import numpy as np
import string as st
import statsmodels.formula.api as smf
from openpyxl import Workbook, load_workbook

from astropy.io import fits
from astropy.time import Time
from astropy.table import Table, hstack
from astropy.visualization.mpl_normalize import ImageNormalize
from astropy.visualization import ContrastBiasStretch
from astroquery.irsa import Irsa
import astropy.units as u
from astropy.coordinates import match_coordinates_sky, \
    match_coordinates_3d, SkyCoord

from photutils import CircularAperture, aperture_photometry
from photutils import background
from photutils import detect_threshold, detect_sources
from astropy.convolution import Gaussian2DKernel
from astropy.stats import gaussian_fwhm_to_sigma


# Lendo a entrada
f = open('input.txt')

FilB = f.readline().strip().split(': ')[1].split(' ')
if FilB[0] == 'n':
    FilB = FilB[0]
else:
    laux = []
    for i in FilB[1:]:
        laux.append(FilB[0]+i)
    FilB = laux
print(FilB)
FilV = f.readline().strip().split(': ')[1].split(' ')
if FilV[0] == 'n':
    FilV = FilV[0]
else:
    laux = []
    for i in FilV[1:]:
        laux.append(FilV[0]+i)
    FilV = laux
print(FilV)
FilR = f.readline().strip().split(': ')[1].split(' ')
if FilR[0] == 'n':
    FilR = FilR[0]
else:
    laux = []
    for i in FilR[1:]:
        laux.append(FilR[0]+i)
    FilR = laux
print(FilR)
Cor = f.readline().strip().split(': ')[1].split(' ')[0].strip()
print(Cor)
rAperture = np.int(f.readline().strip().split(': ')[1])
print(rAperture)
outname = f.readline().strip().split(': ')[1]
print(outname)
buscar = f.readline().strip().split(': ')[1]
print(buscar)
answ = f.readline().strip().split(': ')[1]
print(answ)
f.close()
print('Entrada lida.')

# Abrindo os filtros e o _corr.fits_


def perfom_input(fil, tipo):
    global Im, JD, Filter

    N = len(fil)
    Im[tipo] = []
    JD[tipo] = []
    for i in range(N):
        Im[tipo].append(fits.open(fil[i])[0].data)
        JD[tipo].append(Time(fits.open(fil[i])[0].header['DATE-OBS']))
    Filter[tipo] = tipo

Im = {}
JD = {}
Filter = {}
if not FilB == 'n':
    perfom_input(FilB, 'b')
perfom_input(FilV, 'v')
if not FilR == 'n':
    perfom_input(FilR, 'r')

cordata = Table(Table(fits.open(Cor)[1].data)[['field_x',
                                               'field_y',
                                               'field_ra',
                                               'field_dec']],
                names=['x', 'y', 'ra', 'dec'])

print('Imagens e coordenadas carregadas.')


def onclose(event):
    fig.savefig(outname+'.png')


# Plotando imagem com centro�des do _corr.fits_
def onclick(event):
    # Sair caso o botao esquerdo for pressionado
    b = event.button
    if b == 3:
        fig.canvas.mpl_disconnect(cid)
        plt.close(1)
        return None
    # global ix, iy
    ix, iy = event.xdata, event.ydata
    # print event.button
    font = {'family': 'serif',
            'color':  'yellow',
            'weight': 'normal',
            'size': 10,
            }

    # assign global variable to access outside of function
    global coords, n
    coords.append([ix, iy, 0])
    ax.text(ix, iy, str(n), fontdict=font)
    n = n+1
    fig.canvas.draw_idle()
    # Disconnect after 6 clicks
    if len(coords) == 30:
        fig.canvas.mpl_disconnect(cid)
        plt.close(1)
    return None


def toxlsx(tabela, nome):
    wb = Workbook()
    ws = wb.active

    saida = tabela.as_array()
    ws.append(tabela.colnames)
    for a in saida:
        ws.append(a.tolist())
    wb.save(nome)


def showimg(img, con, bia):
    fig, ax = plt.subplots()
    norm = ImageNormalize(stretch=ContrastBiasStretch(con, bia))
    im = ax.imshow(img, norm=norm, cmap='Greys_r')

    # reinserindo a normaliza��o a fim de deixar a figura exibindo corretamente
    # n�o sei porque precisa disso mas foi necess�rio com alguns arquivos

    norm = ImageNormalize(stretch=ContrastBiasStretch(con, bia))
    im.set_norm(norm)
    fig.canvas.draw_idle()

    return (fig, ax, im)


# Ajustar a normaliza��o da imagem
contr = 50
bias = 0.0


def normalizacao(Img, titulo):

    global contr, bias
    
    def update(val):

        global contr, bias
        
        norm = ImageNormalize(stretch=ContrastBiasStretch(scontr.val,
                                                          sbias.val))
        contr = scontr.val
        bias = sbias.val
        
        ax.set_title('C:'+str(scontr.val)+'B:'+str(sbias.val))
        im.set_norm(norm)
        fig.canvas.draw_idle()
    
    fig, ax, im = showimg(Img, contr, bias)
    ax.set_title('Ajuste o contraste do '+titulo+'.')
    axcolor = 'lightgoldenrodyellow'
    axcontr = plt.axes([0.25, 0.06, 0.65, 0.03], facecolor=axcolor)
    scontr = Slider(axcontr, 'c', -50, 50, valinit=contr)
    axbias = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=axcolor)
    sbias = Slider(axbias, 'b', -0.5, 0.5, valinit=bias)
    scontr.on_changed(update)
    sbias.on_changed(update)
    plt.show()

    return (scontr.val, sbias.val)

# Normalizando as imagens
# os ajustes s�o listas em que o primeiro elemento � c�u
# e o segundo elemento � o objeto.


def perfom_norm(Im, tipo):
    N = len(Im)

    lista = []
    for i in range(N):
        lista.append(normalizacao(Im[i], tipo+str(i)))

    return lista

adjst = {}
if(FilB != 'n'):
    adjst['b'] = perfom_norm(Im['b'], 'B')

adjst['v'] = perfom_norm(Im['v'], 'V')

if(FilR != 'n'):
    adjst['r'] = perfom_norm(Im['r'], 'R')

# Estimando o c�u de forma aut�noma
def bkgExtract(img):
    """
    Extrair o c�u da imagem.
    """
    dila = np.ones((11, 11))
    sigma = 2.0 * gaussian_fwhm_to_sigma    # FWHM = 2.
    kernel = Gaussian2DKernel(sigma, x_size=rAperture, y_size=rAperture)
    kernel.normalize()
    shape = (2*rAperture, 2*rAperture)

    # bkg = Background(img, shape, filter_shape=(3,3))
    threshold = detect_threshold(img, snr=3.)
    for i in range(0, 2):
        # threshold = bkg.background + (3. * bkg.background_rms)
        segm = detect_sources(img, threshold, npixels=5, filter_kernel=kernel)
        mask = ndimage.binary_dilation(segm.data.astype(np.bool),
                                       dila, iterations=3)
        threshold = detect_threshold(img, snr=3., mask=mask)
        bkg = background(img, shape,
                         filter_shape=(rAperture, rAperture), mask=mask)

    # bkg = Background(img, (14,14), filter_shape=(7,7))
    return img-bkg.background

# C�u clicando

# Definindo c�u por meio de cliques
coords = []
n = 1
# fig = plt.figure()
# ax = fig.add_subplot(111)


def click_bkg_extract(img, tipo, c, b, takecoords):
    global coords, coordO, coordS, n, fig, ax

    if takecoords == 'yes':
        # coords = []
        n = 1

        fig, ax, im = showimg(img, c, b)
        
        ax.set_title('Clique no ceu '+tipo+'.')

        fig.canvas.mpl_connect('button_press_event', onclick)
        plt.show()

    if len(coords) < 3:
        img = bkgExtract(img)
        Bkg = 0.0
    else:
        positions = ([C[0] for C in coords], [C[1] for C in coords])
        apertures = CircularAperture(positions, r=rAperture)
        Bkg = np.median(aperture_photometry(img, apertures)['aperture_sum'])

    return Bkg


Bkg = {}
# As coordenadas ser�o fornecidas pela �ltima imagem V
N = len(Im['v'])-1
Bkg['v'] = []
print(adjst['v'])
Bkg['v'].append(click_bkg_extract(Im['v'][N], 'imagem V'
                                  + str(N),
                                  adjst['v'][N][0],
                                  adjst['v'][N][1],
                                  'yes'))

for i in range(N):
    Bkg['v'].append(click_bkg_extract(Im['v'][i], 'imagem V'
                                      + str(i),
                                      adjst['v'][i][0],
                                      adjst['v'][i][1],
                                      'no'))
if FilB != 'n':
    Bkg['b'] = []
    for i in range(len(Im['b'])):
        Bkg['b'].append(click_bkg_extract(Im['b'][i],
                                          'imagem B' + str(i),
                                          adjst['b'][i][0],
                                          adjst['b'][i][1],
                                          'no'))
if FilR != 'n':
    Bkg['r'] = []
    for i in range(len(Im['r'])):
        Bkg['r'].append(click_bkg_extract(Im['r'][i],
                                          'imagem R' + str(i),
                                          adjst['r'][i][0],
                                          adjst['r'][i][1],
                                          'no'))


# Definindo coordenadas de interesse pela �ltima imagem V
positions = (cordata['x'], cordata['y'])
apertures = CircularAperture(positions, r=rAperture)

coords = []
n = 1

fig, ax, im = showimg(Im['v'][-1],adjst['v'][-1][0],adjst['v'][-1][1])

apertures.plot(color='yellow', lw=1, alpha=0.5)
ax.set_title('Selecione as centroides desejadas, feche caso queira usar todas')

cid0 = fig.canvas.mpl_connect('button_press_event', onclick)
cid1 = fig.canvas.mpl_connect('close_event', onclose)
plt.show()


# Reduzindo para as coordenadas do _corr.fits_

if len(coords) > 3:
    c = SkyCoord([(x, y, 0) for x, y in zip(cordata['x'], cordata['y'])],
                 unit=u.pixel, representation='cartesian')
    m = SkyCoord(coords, unit=u.pixel, representation='cartesian')
    idx, a2d, d3d = match_coordinates_3d(m, c)
    cordata = cordata[idx]


# Definindo coordenadas do objeto B

def select_object(img, tipo, c, b):
    global coords, n, fig, ax

    coords = []
    n = 1

    fig, ax, im = showimg(img, c, b)

    ax.set_title('Selecione o objeto '+tipo+'.')

    fig.canvas.mpl_connect('button_press_event', onclick)
    plt.show()

    return (coords[0][0], coords[0][1])

# Definindo coordenadas do objeto B
if(FilB != 'n'):
    oposB = []
    for i in range(len(Im['b'])):
        oposB.append(select_object(Im['b'][i], 'B' + str(i),
                                   adjst['b'][i][0], adjst['b'][i][1]))
# Definindo coordenadas do objeto V
oposV = []
for i in range(len(Im['v'])):
    oposV.append(select_object(Im['v'][i], 'V' + str(i),
                               adjst['v'][i][0], adjst['v'][i][1]))
# Definindo coordenadas do objeto R
if(FilR != 'n'):
    oposR = []
    for i in range(len(Im['r'])):
        oposR.append(select_object(Im['r'][i], 'R' + str(i),
                                   adjst['r'][i][0], adjst['r'][i][1]))

# Fotometria de abertura
positions = (cordata['x'], cordata['y'])
arest = 2.*rAperture
# apertures = RectangularAperture(positions,w=arest,h=arest,theta=0)
apertures = CircularAperture(positions, r=rAperture)


APSum = []
ObSum = []
i = 0
try:
    for i in range(len(Im['b'])):
        # Stars
        aux = aperture_photometry(Im['b'][i], apertures)['aperture_sum']
        aux = -2.5*np.log10(aux-Bkg['b'][i])
        aux.name = 'flux_blue_'+str(i)
        APSum.append(aux)
        # Object
        oaper = CircularAperture(oposB[i], r=rAperture)
        flux = aperture_photometry(Im['b'][i], oaper)['aperture_sum']
        flux = -2.5*np.log10(flux-Bkg['b'][i])
        flux.name = 'flux_blue_'+str(i)
        ObSum.append(flux)
except:
    None
try:
    for i in range(len(Im['v'])):
        # Stars
        aux = aperture_photometry(Im['v'][i], apertures)['aperture_sum']
        aux = -2.5*np.log10(aux-Bkg['v'][i])
        aux.name = 'flux_visible_'+str(i)
        APSum.append(aux)
        # Object
        oaper = CircularAperture(oposV[i], r=rAperture)
        flux = aperture_photometry(Im['v'][i], oaper)['aperture_sum']
        flux = -2.5*np.log10(flux-Bkg['v'][i])
        flux.name = 'flux_visible_'+str(i)
        ObSum.append(flux)
except:
    None
try:
    for i in range(len(Im['r'])):
        # Stars
        aux = aperture_photometry(Im['r'][i], apertures)['aperture_sum']
        aux = -2.5*np.log10(aux-Bkg['r'][i])
        aux.name = 'flux_red_'+str(i)
        APSum.append(aux)
        # Object
        oaper = CircularAperture(oposR[i], r=rAperture)
        flux = aperture_photometry(Im['r'][i], oaper)['aperture_sum']
        flux = -2.5*np.log10(flux-Bkg['r'][i])
        flux.name = 'flux_red_'+str(i)
        ObSum.append(flux)
except:
    None
APSum = hstack([cordata, Table(data=APSum)])
ObSum = Table(data=ObSum)

# raio=7
# xi=cordata['x']-raio; xi=xi.astype('int')
# xf=cordata['x']+raio+1; xf=xf.astype('int')
# yi=cordata['y']-raio; yi=yi.astype('int')
# yf=cordata['y']+raio+1; yf=yf.astype('int')

# i=4
# plt.imshow(data[yi[i]:yf[i],xi[i]:xf[i]],cmap='Greys_r')


# Busca no cat�logo

# Calculando o centro e raio
p = (np.mean(APSum['ra']), np.mean(APSum['dec']))
r = SkyCoord.separation(SkyCoord(p[0], p[1], unit='deg'),
                        SkyCoord(p[0], p[1], unit='deg'))
radec = APSum[['ra', 'dec']].as_array()
for ra, dec in radec:
    aux = SkyCoord.separation(SkyCoord(p[0], p[1], unit='deg'),
                              SkyCoord(ra, dec, unit='deg'))
    if aux > r:
        r = aux

r = r+0.1*r
print(2*r)
# Busca
if buscar == 'Sim':
    print('Buscando no catalogo')
    Q = Irsa.query_region(SkyCoord(p[0], p[1], unit=(u.deg, u.deg)),
                          catalog='fp_psc', radius=r)
    # spatial='Box',width=2.0*r)
    Q.write(outname+'_querytable.csv', format='csv')
    # toxlsx(Q,outname+'_querytable.xlsx')
else:
    Q = Table.read(outname+'_querytable.csv', format='csv')


m = SkyCoord(ra=APSum['ra'], dec=APSum['dec'], unit=u.deg)
c = SkyCoord(ra=Q['ra'], dec=Q['dec'], unit=u.deg)
idx, d2d, d = match_coordinates_sky(m, c)
APSum.add_columns([Q['j_m'][idx], Q['k_m'][idx]])
print(d)

# Retirando estrelas que est�o fora do intervalo de validade da convers�o
# print APSum
lrow = range(len(APSum))
to_remove = []
for i in lrow:
    J_K = APSum[i]['j_m']-APSum[i]['k_m']
    # print J_K
    if((J_K <= -0.1) | (J_K >= 1.0)):
        to_remove.append(i)
APSum.remove_rows(to_remove)

# �ndices restantes

J_K = APSum['j_m']-APSum['k_m']
J = APSum['j_m']

# �ndices das estrelas
B_V = 0.2807*J_K**3-0.4535*J_K**2+1.7006*J_K+0.0484
V_R = 0.3458*J_K**3-0.5401*J_K**2+1.0038*J_K+0.0451
# R_I = 1.077*(J_K**3)-1.6902*(J_K**2)+2.1652*J_K+0.0856 + (V_R)
R_I = 1.4228*J_K**3-2.2303*J_K**2+3.169*J_K+0.1307
V = J+0.1496+3.5143*J_K-2.325*J_K**2+1.4688*J_K**3
B = B_V + V

B_V.name = 'B-V'
V_R.name = 'V-R'
R_I.name = 'R-I'
V.name = 'V'
B.name = 'B'

APSum.add_columns([B, B_V, V_R, R_I, V])

if(FilB != 'n'):
    for i in range(len(Im['b'])):
        b_v = APSum['flux_blue_'+str(i)]-APSum['flux_visible_'+str(i)]
        B_b = B-APSum['flux_blue_'+str(i)]
        b_v.name = 'b-v_'+str(i)
        B_b.name = 'B-b_'+str(i)
        APSum.add_columns([B_b, b_v])
if(FilR != 'n'):
    for i in range(len(Im['r'])):
        v_r = APSum['flux_visible_'+str(i)]-APSum['flux_red_'+str(i)]
        v_r.name = 'v-r_'+str(i)
        APSum.add_column(v_r)
# r_i=Column(data=r-i,name='r-i')
for i in range(len(Im['v'])):
    V_v = V-APSum['flux_visible_'+str(i)]
    V_v.name = 'V-v_'+str(i)
    APSum.add_column(V_v)


# ## Regress�o
def getcoef(data, x, y, q, p):
    data[x].name = 'x'
    data[y].name = 'y'

    mod = smf.quantreg('y ~ x', data)
    res = mod.fit(q=q)
    if p:
        X = np.linspace(min(data['x']), max(data['x']), 50)
        plt.plot(data['x'], data['y'], 'o')
        plt.plot(X, res.params['x']*X+res.params['Intercept'])
        plt.xlabel(x)
        plt.ylabel(y)
        plt.show(1)
        # fig.canvas.draw_idle()
    return res.params['x']

a = False
if answ == 'Sim':
    a = True

g = open(outname+'_coef.txt', 'w')
if(FilB != 'n'):
    for i in range(len(Im['b'])):
        aux = getcoef(APSum[['B-V', 'b-v_'+str(i)]],
                      'B-V', 'b-v_'+str(i), 0.5, a)
        if(aux == 0):
            print('Divisao por zero')
        else:
            Tbv = 1.0/aux
            Tb = getcoef(APSum[['B-V', 'B-b_'+str(i)]],
                         'B-V', 'B-b_'+str(i), 0.5, a)
            print('Coef(b-v)_'+str(i)+' = ', Tbv, file=g)
            print('Coef(B-b)_'+str(i)+' = ', Tb, file=g)
        sys.stdout.flush()

if(FilR != 'n'):
    for i in range(len(Im['r'])):
        Tvr = 1./getcoef(APSum[['V-R', 'v-r_'+str(i)]],
                         'V-R', 'v-r_'+str(i), 0.5, a)
        print('Coef(v-r)_'+str(i)+' = ', Tvr, file=g)
        sys.stdout.flush()

        Tv = getcoef(APSum[['V-R', 'V-v_'+str(i)]],
                     'V-R', 'V-v_'+str(i), 0.5, a)
        print('Coef(V-v)'+str(i)+' = ', Tv, file=g)
g.close()

# Indices com o objeto
for i in range(len(Im['v'])):
    vo = ObSum['flux_visible_'+str(i)]
    if(FilB != 'n'):
        b_vo = ObSum['flux_blue_'+str(i)]-vo
        B_Vo = B_V+Tbv*(b_vo-b_v)
        B_Vo.name = 'B-V obj_'+str(i)
        Bo = ObSum['flux_blue_'+str(i)] + APSum['B-b_'+str(i)] \
            + Tb*(B_Vo-APSum['B-V'])
        Bo.name = 'B obj_'+str(i)
        Vo = Bo - B_Vo
        Vo.name = 'V obj_'+str(i)
        APSum.add_columns([Bo, B_Vo])

    if(FilR != 'n'):
        v_ro = vo-ObSum['flux_red_'+str(i)]
        V_Ro = V_R+Tvr*(v_ro-v_r)
        V_Ro.name = 'V-R obj_'+str(i)
        Vo = vo+V_v+Tv*(V_Ro-V_R)
        Vo.name = 'V obj_'+str(i)
        APSum.add_column(V_Ro)
    APSum.add_column(Vo)


# APSum.write(outname+'_dados.csv',format='csv')
# Gravando xlsx
toxlsx(APSum, outname+'_dados.xlsx')
toxlsx(ObSum, outname+'_objeto.xlsx')

wb = load_workbook(outname+'_dados.xlsx')
ws = wb.active
ws.append(['Filter', 'JD'])
for i in JD.keys():
    linha = [Filter[i]]
    for j in JD[i]:
        linha.append(j.jd)
    ws.append(linha)
wb.save(outname+'_dados.xlsx')
