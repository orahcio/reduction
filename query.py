import pandas as pd
import astropy.units as u
from astroquery.irsa import Irsa
from astropy.coordinates import match_coordinates_sky, match_coordinates_3d, SkyCoord

def ler_input(inputfile):
    reg = {}
    filtype = ['b','v','r']
    with open(inputfile) as f:
        for fi in range(3):
            fil = f.readline().strip().split(': ')[1].split(' ')
            if fil[0] == 'n':
                reg[filtype[fi]] = []
            else:
                laux = []
                for i in fil[1:]:
                    laux.append(fil[0]+i)
                reg[filtype[fi]] = laux
        
        reg['Cord'] = f.readline().strip().split(': ')[1].split(' ')[0].strip()
        reg['radius'] = int(f.readline().strip().split(': ')[1])
        reg['out'] = f.readline().strip().split(': ')[1]
        reg['query'] = f.readline().strip().split(': ')[1]
        reg['fitplot'] = f.readline().strip().split(': ')[1]
    
    return reg

# Lendo a entrada
reg = ler_input('input.txt')
r = reg['radius']

table = pd.read_excel(reg['out']+'_indices.xlsx')
odata = pd.read_excel(reg['out']+'_objeto.xlsx')

# Query data from 2MASS
p = (table['field_ra'].mean(), table['field_dec'].mean())
r = SkyCoord.separation(SkyCoord(p[0],p[1],unit='deg'),SkyCoord(p[0],p[1],unit='deg'))
for ra, dec in zip(table['field_ra'],table['field_dec']):
    aux = SkyCoord.separation(SkyCoord(p[0], p[1], unit='deg'),\
        SkyCoord(ra, dec, unit='deg'))
    if aux>r:
        r=aux
r += 0.1*r

if reg['query'] == 'Sim':
    print('Buscando no catalogo')
    Q = Irsa.query_region(SkyCoord(table['field_ra'][0], table['field_dec'][0], unit='deg'),\
        catalog='fp_psc', radius=10*u.arcse).to_pandas().iloc[0]
    Q = pd.DataFrame(Q).transpose()
    for ra, dec in zip(table['field_ra'][1:],table['field_dec'][1:]):
        aux = Irsa.query_region(SkyCoord(ra, dec, unit='deg'),\
            catalog='fp_psc', radius=10*u.arcsec).to_pandas().iloc[0]
        Q = Q.append(aux)
    Q.to_excel(reg['out']+'_querytable.xlsx')
else:
    Q = pd.read_excel(reg['out']+'_querytable.xlsx')

print(Q)

# m = SkyCoord(ra=table['field_ra'], dec=table['field_dec'], unit='deg')
# c = SkyCoord(ra=Q['ra'], dec=Q['dec'], unit='deg')
# idx, a, b = match_coordinates_sky(m, c)

# tabela = pd.DataFrame({})

# tabela['ra'] = table['field_ra'].values
# tabela['dec'] = table['field_dec'].values
# tabela['ra_q'] = Q.iloc[idx]['ra'].values
# tabela['dec_q'] = Q.iloc[idx]['dec'].values
# tabela['j_m'] = Q.iloc[idx]['j_m'].values
# tabela['k_m'] = Q.iloc[idx]['k_m'].values



# print(a,b)
# tabela.to_excel(reg['out']+'debug.xlsx')
