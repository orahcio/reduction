#!./bin/python

import sys
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import numpy as np
import statsmodels.formula.api as smf

from astropy.io import fits
from astropy.time import Time
from astropy.table import Table, vstack
from astropy.visualization.mpl_normalize import ImageNormalize
from astropy.visualization import ContrastBiasStretch
import astropy.units as u
from astroquery.irsa import Irsa
from astropy.coordinates import match_coordinates_sky, match_coordinates_3d, SkyCoord

from photutils import CircularAperture, aperture_photometry, find_peaks
from photutils.centroids import centroid_sources, centroid_1dg, centroid_2dg
from astropy.stats import sigma_clipped_stats

import pandas as pd


class idata(object):
    def __init__(self, fitsfile=None, fil='', master=None, r=11):
        if fitsfile==None:
            print('Forneça um fits para informações')
            sys.exit(-1)
        
        self.exptime = fits.getheader(fitsfile)['EXPTIME']
        self.data = fits.getdata(fitsfile)/self.exptime
        self.time = Time(fits.getheader(fitsfile)['DATE-OBS'])

        if fil=='':
            self.fil = fits.getheader(fitsfile)['FILTERS'].strip()
        _, self.bkg, self.bkg_std = sigma_clipped_stats(self.data, sigma=3.0) # primeira estimativa de céu é a mediana calculada acima
        self.stars = None
        self.objec = None
        if master!=None: # copia os atributos de estrelas e objeto de um mestre
            self.coords = self.centroide(master.coords,r)
            self.coordo = self.centroide(master.coordo,r)
            self.coordy = master.coordy
            self.estimate_sky(r) # estima o céu a partir dos pontos dados acima
            self.contr = master.contr
            self.bias = master.bias
        else: # caso não seja fornecido o parâmetro mestre um valor inicial é fornecido ou coletado
            self.coords = []
            self.coordo = []
            self.coordy = []
            # faz uma primeira estimativa do céu, importante para melhorar a centralização
            self.contr = 1.
            self.bias = 0.
    

    def centroide(self, coord=[], r=11):
        '''Centralizar coordenadas fornecidas'''

        caux = []
        for x,y in coord:

            aux2 = centroid_sources(self.data-self.bkg,x,y)
            newx,newy = aux2[0][0], aux2[1][0]

            #a,b = len(self.data),len(self.data[0])
            #mask = CircularAperture([newx,newy],r).to_mask(method='center')
            #mdata = mask.multiply(self.data)
            #threshold = self.bkg+5*self.bkg_std
            #newx, newy = find_peaks(mdata, threshold)[['x_peak','y_peak']][0]

            #newx += x-r
            #newy += y-r

            aux2 = centroid_sources(self.data-self.bkg,newx,newy,centroid_func=centroid_1dg)
            newx,newy = aux2[0][0], aux2[1][0]

            aux = centroid_sources(self.data-self.bkg,newx,newy,centroid_func=centroid_2dg)
            newx,newy = aux[0][0], aux[1][0]

            caux.append([newx,newy])
        
        return caux

    def plot(self, cor, title='', r=11, color='yellow'):

        def update(val):
            self.contr = scontr.val
            self.bias = sbias.val
            norm = ImageNormalize(stretch=ContrastBiasStretch(self.contr, self.bias))
            self.im.set_norm(norm)


        #table = Table.read(cor,hdu=1)[['index_x','index_y']].to_pandas()
        t = pd.DataFrame(fits.getdata(cor))
        table = pd.DataFrame({'index_x': t['index_x'], 'index_y': t['index_y']})
        apertures = CircularAperture(table,r)
        
        plt.close('all')
        self.fig, self.ax = plt.subplots()
        self.ax.set_title(title+'\n'+self.fil+' em '+self.time.value)
        c = self.contr
        b = self.bias
        norm = ImageNormalize(stretch=ContrastBiasStretch(c, b))
        self.im = self.ax.imshow(self.data, norm=norm, cmap='Greys')
        if not apertures==None:
            apertures.plot(color=color, lw=1, alpha=0.5)

        
        axcolor = 'lightgoldenrodyellow'
        axcontr = self.fig.add_axes([0.025, 0.3, 0.03, 0.3], facecolor=axcolor)
        scontr = Slider(axcontr, 'c', -50, 50, valinit=self.contr, valstep=0.003, orientation='vertical')
        axbias = self.fig.add_axes([0.07, 0.3, 0.03, 0.3], facecolor=axcolor)
        sbias = Slider(axbias, 'b', -0.5, 0.5, valinit=self.bias, valstep=0.003, orientation='vertical')
        
        # Conectando eventos
        scontr.on_changed(update)
        sbias.on_changed(update)
        if title!='':
            self.n = 0
            self.cid = self.fig.canvas.mpl_connect('button_press_event', self.onclick)

        mng = plt.get_current_fig_manager()
        mng.resize(*mng.window.maxsize())
        plt.show()

        if title=='Estrealas':
            self.select_stars(r)
        if title=='Objeto':
            self.select_object(r)
        if title=='Céu':
            self.select_sky(r)

    

    def normalizar(self):
    
        def update(val):
            self.contr = scontr.val
            self.bias = sbias.val
            norm = ImageNormalize(stretch=ContrastBiasStretch(self.contr, self.bias))
            self.im.set_norm(norm)
            # self.fig.canvas.draw_idle()
        
        #self.plot(title='Ajuste os parâmetros pra normalizar\n'+self.fil+' em '+self.time.value)
        #self.select_object(r)
        axcolor = 'lightgoldenrodyellow'
        axcontr = self.fig.add_axes([0.025, 0.3, 0.03, 0.3], facecolor=axcolor)
        scontr = Slider(axcontr, 'c', -50, 50, valinit=self.contr, valstep=0.003, orientation='vertical')
        axbias = self.fig.add_axes([0.07, 0.3, 0.03, 0.3], facecolor=axcolor)
        sbias = Slider(axbias, 'b', -0.5, 0.5, valinit=self.bias, valstep=0.003, orientation='vertical')
        scontr.on_changed(update)
        sbias.on_changed(update)


    def onclick(self, event):
        # Sair caso o botao esquerdo for pressionado
        b = event.button
        if b == 3:
            self.fig.canvas.mpl_disconnect(self.cid)
            plt.close('all')
            return None
        # global ix, iy
        ix, iy = event.xdata, event.ydata
        # print event.button
        font = {'family': 'serif',
                'color':  'yellow',
                'weight': 'normal',
                'size': 10,
                }
        
        if event.inaxes==self.ax: # coloca coordenadas se estiver dentro da imagem
            self.coords.append([ix, iy])
            self.n += 1
            self.ax.text(ix, iy, str(self.n), fontdict=font)
            self.fig.canvas.draw_idle()
        
        # Disconnect after 30 clicks
        if len(self.coords) == 30: # impondo um máximo de estrelas
            self.fig.canvas.mpl_disconnect(self.cid)
            plt.close('all')
        return None


    def select_stars(self, r=11):

        # Ajustar as centroides
        self.coords = self.centroide(self.coords,r)


    def select_object(self, r=11):

        self.coordo = [self.coords.pop()]

        # Ajustar as centroides
        self.coordo = self.centroide(self.coordo,r)
    

    def select_sky(self, r=11):

        self.coordy = [self.coords.pop() for i in range(self.n)]
        if len(self.coordy)>0:
            self.estimate_sky(r)


    def estimate_sky(self,r):
        
        apertures = CircularAperture(self.coordy, r)
        masks = apertures.to_mask(method='center')
        datasky = []
        for i in range(len(self.coordy)):
            aperdata = masks[i].multiply(self.data)
            mask = masks[i].data
            datasky += aperdata[mask>0].tolist()
        
        _, self.bkg, self.bkg_std = sigma_clipped_stats(datasky)
    

    def check_apertures_ok(self, r=11):

        self.fig, self.ax = plt.subplots()
        #self.ax.set_title(title+'\n'+self.fil+' em '+self.time.value)
        c = self.contr
        b = self.bias
        norm = ImageNormalize(stretch=ContrastBiasStretch(c, b))
        self.im = self.ax.imshow(self.data, norm=norm, cmap='Greys')

        coord = [[v[0],v[1]] for v in self.coordo+self.coords]

        apertures = CircularAperture(coord,r)
        apertures.plot(color='orange', lw=1, alpha=0.5)
        self.normalizar()

        mng = plt.get_current_fig_manager()
        mng.resize(*mng.window.maxsize())
        plt.show()

    def aperture_photometry(self, r=11):

        # if len(self.coordy)>0:
            # apertures = CircularAperture(self.coordy, r)
            # ceu = np.median(aperture_photometry(self.data,apertures).to_pandas()['aperture_sum'].values)

        if len(self.coords)>0:
            apertures = CircularAperture(self.coords, r)
            self.stars = aperture_photometry(self.data-self.bkg,apertures,\
                a.bkg_std*np.ones(shape=self.data.shape)).to_pandas()
            # self.stars['aperture_sum'] -= ceu
        if len(self.coordo)>0:
            apertures = CircularAperture(self.coordo, r)
            self.objec = aperture_photometry(self.data-self.bkg,apertures,\
                a.bkg_std*np.ones(shape=self.data.shape)).to_pandas()
            # self.objec['aperture_sum'] -= ceu


def ler_input(inputfile):
    reg = {}
    filtype = ['b','v','r']
    with open(inputfile) as f:
        for fi in range(3):
            fil = f.readline().strip().split(': ')[1].split(' ')
            if fil[0] == 'n':
                reg[filtype[fi]] = []
            else:
                laux = []
                for i in fil[1:]:
                    laux.append(fil[0]+i)
                reg[filtype[fi]] = laux
        
        reg['Cord'] = f.readline().strip().split(': ')[1].split(' ')[0].strip()
        reg['radius'] = int(f.readline().strip().split(': ')[1])
        reg['out'] = f.readline().strip().split(': ')[1]
        reg['query'] = f.readline().strip().split(': ')[1]
        reg['fitplot'] = f.readline().strip().split(': ')[1]
    
    return reg


def mag(F,err):

    mag = -2.5*np.log10(F)
    mag_err = 2.5/F*err

    return mag, mag_err


def getcoef(data,x,y,q=0.5,p='Não'):
    aux = data[[x,y]].rename(columns={x:'x',y:'y'})
    mod = smf.quantreg('y ~ x', aux)
    res = mod.fit(q=q)
    if p=='Sim':
        X = np.linspace(aux['x'].min(), aux['x'].max(), 50)
        plt.plot(aux['x'], aux['y'], 'o')
        plt.plot(X, res.params['x']*X+res.params['Intercept'])
        plt.xlabel(x)
        plt.ylabel(y)
        plt.show()
    return res.params['x']


# Lendo a entrada
reg = ler_input('input.txt')
r = reg['radius']

try:
    table = pd.read_excel(reg['out']+'_starphot.xlsx')
    odata = pd.read_excel(reg['out']+'_objcphot.xlsx')
except FileNotFoundError:
    ### ---------------- INÍCIO DA FOTOMETRIA ------------------------
    # Cada quadro conterá toda informação necessária para a fotometria
    frame = {'b': [], 'v': [], 'r': []}
    frame['v'].append(idata(reg['v'][0])) # Começando pelo primeiro arquivo do visível
    a = frame['v'][0]

    # Selecionar pontos do céu para estimar seu valor
    a.plot(reg['Cord'],'Céu',r)

    # Seleciona o objeto
    a.plot(reg['Cord'],'Objeto',r)
    # Selecionar estrelas de comparacão
    a.plot(reg['Cord'],'Estrela',r)

    a.aperture_photometry(r)

    # Cria as demais imagens copiando os parâmetros da imagem mestre
    for i in range(1,len(reg['v'])):
        frame['v'].append(idata(reg['v'][i],master=a,r=r))
        a = frame['v'][-1]
        a.plot(reg['Cord'],'Objeto',r)
        a.aperture_photometry(r)

    for i in ['b','r']:
        for j in range(len(reg[i])):
            frame[i].append(idata(reg[i][j],master=a,r=r))
            a = frame[i][-1]
            a.plot(reg['Cord'],'Objeto',r)
            a.aperture_photometry(r)

    # Checando se tá tudo ok
    # for i in ['b','v','r']:
    #     for a in frame[i]:
    #         a.check_apertures_ok(r)

    # Começando a montar a grande tabela que irá conter todos os índices
    # table = Table.read(reg['Cord']).to_pandas()[['index_x','index_y','field_ra','field_dec']]
    t = pd.DataFrame(fits.getdata(reg['Cord']))
    table = pd.DataFrame({'field_x': t['field_x'].values.byteswap().newbyteorder(),\
        'field_y': t['field_y'].values.byteswap().newbyteorder(),\
            'field_ra': t['field_ra'].values.byteswap().newbyteorder(),\
                'field_dec': t['field_dec'].values.byteswap().newbyteorder()})

    # usar as coordenadas da primeira imagem V para acessar o catálogo
    cartesian = [(x,y,0) for x,y in frame['v'][0].coords]
    coord = SkyCoord(cartesian, representation_type='cartesian') # coordenadas do filtro V, primeira imagem
    cartesian = [(x,y,0) for x,y in zip(table['field_x'],table['field_y'])]
    cofit = SkyCoord(cartesian, representation_type='cartesian')
    idx, _, _ = match_coordinates_3d(coord,cofit)

    table = table.iloc[idx]
    odata = pd.DataFrame({})

    for i in ['b','v','r']:
        for j in range(len(frame[i])):
            # Índeces
            cflux = i+'('+str(j)+')'
            cfluxerr = i+'('+str(j)+')_err'
            # Estrelas
            m, merr = mag(frame[i][j].stars['aperture_sum'].values,\
                frame[i][j].stars['aperture_sum_err'].values)
            table[cflux]=m
            table[cfluxerr]=merr
            # Objeto
            m, merr = mag(frame[i][j].objec['aperture_sum'].values,\
                frame[i][j].objec['aperture_sum_err'].values)
            odata[cflux]=m
            odata[cfluxerr]=merr
            odata[cflux+'_bkg']=frame[i][j].bkg

    table.to_excel(reg['out']+'_starphot.xlsx', index=False) # salvando a fotometria da estrela
    odata.to_excel(reg['out']+'_objcphot.xlsx', index=False) # salvando a fotometria do objeto
    ### ---------------- FIM DA FOTOMETRIA ---------------------------


# Query data from 2MASS
j = []
k = []
c_ra = []
c_dec = []
if reg['query'] == 'Sim':

    print('Buscando no catalogo')
    for ra, dec in zip(table['field_ra'],table['field_dec']):
        m = SkyCoord(ra, dec, unit='deg')
        Q = Irsa.query_region(m, catalog='fp_psc',\
            radius=10*u.arcsec, selcols=['ra','dec','j_m','k_m']).to_pandas()
        c = SkyCoord(Q['ra'], Q['dec'], unit='deg')
        idx, _, _ = m.match_to_catalog_sky(c)
        j.append(Q['j_m'][idx])
        k.append(Q['k_m'][idx])
        c_ra.append(Q['ra'][idx])
        c_dec.append(Q['dec'][idx])
    Q = pd.DataFrame({'ra': c_ra, 'dec': c_dec, 'j_m': j, 'k_m': k})
    Q.to_excel(reg['out']+'_querytable.xlsx')
else:
    Q = pd.read_excel(reg['out']+'_querytable.xlsx')

print('Busca concluida')

#table['ra_q'] = Q['ra'].values
#table['dec_q'] = Q['dec'].values
table['j_m'] = Q['j_m'].values
table['k_m'] = Q['k_m'].values

print('Tabela copiada')

# Removendo estrelas que não estão no intervalo de validade
J_K = table['j_m'].values-table['k_m'].values

table = table[(J_K<=1.0)&(J_K>=-0.1)] # estrelas dentro do intervalo de validade

J = table['j_m'].values
J_K = J-table['k_m'].values

# Índices das estrelas
B_V = 0.2807*J_K**3 - 0.4535*J_K**2 + 1.7006*J_K + 0.0484
V_R = 0.3458*J_K**3 - 0.5401*J_K**2 + 1.0038*J_K + 0.0451
V=J + 1.4688*J_K**3 - 2.3250*J_K**2 + 3.5143*J_K + 0.1496
B = B_V + V

table['B-V'] = B_V
table['V-R'] = V_R
table['V'] = V
table['B'] = B

print('Combinar índices')

# Índeces com b
for i in range(len(reg['b'])):
    bflux = 'b('+str(i)+')'
    vflux = 'v('+str(i)+')'
    b = table[bflux].values
    v = table[vflux].values
    table['b-v('+str(i)+')'] = b-v
    table['B-b('+str(i)+')'] = B-b
# Índeces com r
for i in range(len(reg['r'])):
    rflux = 'r('+str(i)+')'
    vflux = 'v('+str(i)+')'
    r = table[rflux].values
    v = table[vflux].values
    table['v-r('+str(i)+')'] = v-r
# Índeces com v
for i in range(len(reg['v'])):
    vflux = 'v('+str(i)+')'
    v = table[vflux].values
    table['V-v('+str(i)+')'] = V-v

print('Coeficientes')
# Obtendo coeficientes para cada índice:
# Com b
Tbv = []
Tb = []
for i in range(len(reg['b'])):
    Tbv.append(1/getcoef(table,'B-V','b-v('+str(i)+')',p=reg['fitplot']))
    Tb.append(getcoef(table,'B-V','B-b('+str(i)+')',p=reg['fitplot']))
if len(Tb):
    pd.DataFrame({'Tbv':Tbv,'Tb':Tb}).to_excel(reg['out']+'_b_coefs.xlsx',index=False)

# Com r
Tvr = []
Tv = []
for i in range(len(reg['r'])):
    Tvr.append(1/getcoef(table,'V-R','v-r('+str(i)+')',p=reg['fitplot']))
    Tv.append(getcoef(table,'V-R','V-v('+str(i)+')',p=reg['fitplot']))
if len(Tv):
    pd.DataFrame({'Tvr':Tvr,'Tv':Tv}).to_excel(reg['out']+'_r_coefs.xlsx',index=False)

print('Com objeto')
# Índices com o objeto

# BV
if (len(reg['b'])>0)&(len(reg['r'])<1):
    for i in range(len(reg['b'])):
        vo = odata['v('+str(i)+')'].values[0]
        bo = odata['b('+str(i)+')'].values[0]
        b_v = table['b-v('+str(i)+')'].values
        B_b = table['B-b('+str(i)+')'].values
        b_vo =  bo-vo

        B_Vo = B_V + Tbv[i]*(b_vo - b_v)
        Bo = bo + B_b + Tb[i]*(B_Vo-B_V)
        Vo = Bo - B_Vo
        
        table['B-V(obj)_'+str(i)] = B_Vo
        table['B(obj)_'+str(i)] = Bo
        table['V(obj)_'+str(i)] = Vo

# VR
if (len(reg['b'])<1)&(len(reg['r'])>0):
    for i in range(len(reg['r'])):
        vo = odata['v('+str(i)+')'].values[0]
        ro = odata['r('+str(i)+')'].values[0]
        v_r = table['v-r('+str(i)+')'].values
        V_v = table['V-v('+str(i)+')'].values
        v_ro = vo-ro

        V_Ro = V_R + Tvr[i]*(v_ro - v_r)
        Vo = vo + V_v + Tv[i]*(V_Ro - V_R)
        Ro = Vo - V_Ro

        table['V-R(obj)_'+str(i)] = V_Ro
        table['R(obj)'+str(i)] = Ro
        table['V(obj)_'+str(i)] = Vo

# BVR
if (len(reg['b'])>0)&(len(reg['r'])>0):
    for i in range(len(reg['v'])):
        vo = odata['v('+str(i)+')'].values[0]
        bo = odata['b('+str(i)+')'].values[0]
        ro = odata['r('+str(i)+')'].values[0]
        b_v = table['b-v('+str(i)+')'].values
        v_r = table['v-r('+str(i)+')'].values
        V_v = table['V-v('+str(i)+')'].values
        b_vo = bo-vo
        v_ro = vo-ro

        V_Ro = V_R+Tvr[i]*(v_ro-v_r)
        Vo = vo + V_v + Tv[i]*(V_Ro - V_R)
        Ro = Vo - V_Ro
        Bo = Vo + B_V + Tbv[i]*(b_vo - b_v)

        table['V-R(obj)_'+str(i)] = V_Ro
        table['V(obj)_'+str(i)] = Vo
        table['R(obj)'+str(i)] = Ro
        table['B(obj)'+str(i)] = Bo


table.to_excel(reg['out']+'_indices.xlsx',index=False)
odata.to_excel(reg['out']+'_objeto.xlsx',index=False)

print('Saída salva')

sys.exit()
